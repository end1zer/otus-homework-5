﻿using Common.Contracts;
using Domain;
using Domain.Entities;


internal sealed class AvitoService : IAvitoService
{
    private readonly IRepository _repository;

    public AvitoService(IRepository repository)
    {
        _repository = repository ?? throw new ArgumentNullException(nameof(repository));
    }

    public async Task AddUserAsync(string name, string surname, string lastName,
        string phoneNumber, int age)
    {
        //NEW FEATURE
        User user = new(name, surname, lastName, phoneNumber, age);
        await _repository.AddUserAsync(user);
    }

    public async Task AddAdvertisementAsync(string title, string description, DateTime creationDate,
        decimal cost, int userId, int categoryId)
    {
        Advertisement advertisement = new(title, description, creationDate, cost, userId, categoryId);
        await _repository.AddAdvertisementAsync(advertisement);
    }

    public async Task AddCategoryAsync(string name)
    {
        Category category = new(name);
        await _repository.AddCategoryAsync(category);
    }

    public async Task ShowTables()
    {
        Console.WriteLine("=========Категории=========\n");
        var categories = await _repository.GetCategoriesAsync();
        foreach (var category in categories)
        {
            Console.WriteLine(GetDataInJson(category));
        }

        Console.WriteLine("=========Пользователи=========\n");
        var users = await _repository.GetUsersAsync();
        foreach (var user in users)
        {
            Console.WriteLine(GetDataInJson(user));
        }

        Console.WriteLine("=========Объявления=========\n");
        var advertisements = await _repository.GetAdvertisementsAsync();
        foreach (var advertisement in advertisements)
        {
            Console.WriteLine(GetDataInJson(advertisement));
        }
    }

    private string GetDataInJson(object instance)
    {
        return Newtonsoft.Json.JsonConvert.SerializeObject(instance);
    }
}
