﻿namespace DTOs
{
    //NEW FEATURE
    public record UserDto
    {
        private readonly string? _fullName;
        public string? FullName
        {
            get => _fullName;
            //NEW FEATURE
            init => _fullName = (value ?? throw new ArgumentNullException(nameof(FullName)));
        }
        public string? PhoneNumber { get; init; }
        public int Age { get; init; }
    }
}
