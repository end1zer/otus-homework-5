﻿using Microsoft.Extensions.DependencyInjection;
using Common.Contracts;
using Persistance;


//NEW FEATURE
var serviceProvider = new ServiceCollection()
                .AddPersistence()
                .AddTransient<IAvitoService, AvitoService>()
                .AddTransient<UserInteraction>()
                .BuildServiceProvider();

var userInteraction = serviceProvider.GetService<UserInteraction>() ?? throw new NullReferenceException(nameof(UserInteraction));

await userInteraction.MainFlowAsync();

return 0;
