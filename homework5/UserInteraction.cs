﻿using Common.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


internal class UserInteraction
{
    private readonly IAvitoService _avitoService;

    public UserInteraction(IAvitoService avitoService)
    {
        _avitoService = avitoService ?? throw new ArgumentNullException(nameof(avitoService));
    }

    public async Task MainFlowAsync()
    {
        Console.WriteLine("Действие:\n1. Показать данные\n2. добавить данные");
        var key = Console.ReadKey();
        switch (key.KeyChar)
        {
            case '1':
                Console.WriteLine("\n");
                await _avitoService.ShowTables();
                return;
            case '2':
                Console.WriteLine("\nТаблица для добавления:\n1. Category\n2. User \n3. Advertisement");
                key = Console.ReadKey();
                switch (key.KeyChar)
                {
                    case '1':
                        {
                            await ReadCategoryAsync();
                        }
                        break;
                    case '2':
                        {
                            await ReadUserAsync();
                        }
                        break;
                    case '3':
                        {
                            await ReadAdvertisementAsync();
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Такого выбора не существует.");
                        }
                        break;
                }
                break;
            default:
                {
                    Console.WriteLine("Такого выбора не существует.");
                    break;
                }
        }
    }

    private async Task ReadCategoryAsync()
    {
        Console.WriteLine("\n=========Добавление новой категории=========");

        Console.WriteLine("\nИмя: ");
        var name = Console.ReadLine() ?? throw new ArgumentNullException("name");

        await _avitoService.AddCategoryAsync(name);
        Console.WriteLine($"Категория добавлена");
    }

    private async Task ReadUserAsync()
    {
        Console.WriteLine("\n=========Добавление нового пользователя=========");

        Console.WriteLine("\nИмя: ");
        var name = Console.ReadLine() ?? throw new ArgumentNullException("name");

        Console.WriteLine("\nФамилия: ");
        var surname = Console.ReadLine() ?? throw new ArgumentNullException("surname");

        Console.WriteLine("\nОтчество: ");
        var lastname = Console.ReadLine() ?? throw new ArgumentNullException("lastname");

        Console.WriteLine("\nНомер телефона: ");
        var phoneNumber = Console.ReadLine() ?? throw new ArgumentNullException("phoneNumber");

        Console.WriteLine("\nВозраст: ");
        var ageStr = Console.ReadLine() ?? throw new ArgumentNullException("age");

        if (int.TryParse(ageStr, out var age) == false)
        {
            Console.WriteLine("Неправильный формат возраста");
            return;
        }

        await _avitoService.AddUserAsync(name, surname, lastname, phoneNumber, age);
        Console.WriteLine($"Пользователь добавлен");
    }

    private async Task ReadAdvertisementAsync()
    {
        Console.WriteLine("\n=========Добавление нового пользователя=========");

        Console.WriteLine("\nЗаголовок: ");
        var title = Console.ReadLine() ?? throw new ArgumentNullException("title");

        Console.WriteLine("\nОписание: ");
        var description = Console.ReadLine() ?? throw new ArgumentNullException("description");

        Console.WriteLine("\nСтоимость: ");
        var costStr = Console.ReadLine() ?? throw new ArgumentNullException("age");

        if (decimal.TryParse(costStr, out var cost) == false)
        {
            Console.WriteLine("Неправильный формат стоимости");
            return;
        }

        Console.WriteLine("\nИдентификатор пользователя: ");
        var userIdStr = Console.ReadLine();

        if (int.TryParse(userIdStr, out var userId) == false)
        {
            Console.WriteLine("Неправильный формат идентификатора пользователя");
            return;
        }

        Console.WriteLine("\nИдентификатор категории: ");
        var categoryIdStr = Console.ReadLine();

        if (int.TryParse(categoryIdStr, out var categoryId) == false)
        {
            Console.WriteLine("Неправильный формат идентификатора категории");
            return;
        }

        await _avitoService.AddAdvertisementAsync(title, description, DateTime.Now, cost, userId, categoryId);
        Console.WriteLine($"Объявление добавлено");
    }
}