﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Contracts
{
    internal interface IAvitoService
    {
        Task AddUserAsync(string name, string surname, string lastName, string phoneNumber, int age);
        Task AddAdvertisementAsync(string title, string description, DateTime creationDate,
            decimal cost, int userId, int categoryId);
        Task AddCategoryAsync(string name);
        Task ShowTables();
    }
}
