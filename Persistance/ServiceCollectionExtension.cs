﻿using Domain;
using Microsoft.Extensions.DependencyInjection;

namespace Persistance
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            services.AddDbContext<AvitoDbContext>();
            services.AddSingleton<IRepository, Repository>();

            return services;
        }
    }
}
