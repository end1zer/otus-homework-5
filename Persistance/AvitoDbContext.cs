﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Persistance
{
    public sealed class AvitoDbContext : DbContext
    {
        public const string DEFAULT_SCHEMA = "avito";

        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }

        public AvitoDbContext(DbContextOptions<AvitoDbContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // NOTE: пусть будет тут, лень выносить в конфиг
            optionsBuilder.UseNpgsql(@"Server=127.0.0.1;User Id=postgres;Password=12345;Port=5432;Database=light_avito;");
        }

        // NOTE: не буду запариваться над выносом в отдельные файлы конфигов
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(DEFAULT_SCHEMA);

            modelBuilder.Entity<Advertisement>(e =>
            {
                e.HasOne(x => x.User)
                    .WithMany(x => x.Advertisements)
                    .HasForeignKey(x => x.UserId)
                    .OnDelete(DeleteBehavior.Cascade);

                e.HasOne(x => x.Category)
                    .WithMany()
                    .HasForeignKey(x => x.CategoryId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}
