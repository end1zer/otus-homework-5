﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace Persistance
{
    public static class ModelBuilderHelper
    {
        static void SetAccessMode<TEntity>(this ModelBuilder modelBuilder, string name)
            where TEntity : class, IEntity
        {
            modelBuilder.Entity<TEntity>().Metadata.FindNavigation(name)
                ?.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
