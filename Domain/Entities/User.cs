﻿using Domain.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("users")]
    public class User : Entity, IEntity
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("surname")]
        public string Surname { get; set; }

        [Column("lastname")]
        public string LastName { get; set; }

        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        [Column("age")]
        public int Age { get; set; }

        [JsonIgnore]
        public ICollection<Advertisement> Advertisements { get; set; }

        public User()
        {
            Advertisements = new HashSet<Advertisement>();
        }

        public User(string name, string surname, string lastName, string phoneNumber, int age)
        {
            Name = name;
            Surname = surname;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            Age = age;
        }
    }
}
