﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("categories")]
    public class Category : Entity, IEntity
    {
        [Column("name")]
        public string Name { get; set; }

        public Category() { }

        public Category(string name)
        {
            //NEW FEATURE
            if (name is not null) { Name = name; }
            throw new ArgumentNullException(nameof(name));
        }
    }
}
