﻿using Domain.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("advertisements")]
    public class Advertisement : Entity, IEntity
    {
        [Column("title")]
        public string Title { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("creation_date")]
        public DateTime CreationDate { get; set; }

        [Column("cost")]
        public decimal Cost { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("category_id")]
        public int CategoryId { get; set; }
        
        [JsonIgnore]
        public virtual User User { get; set; }

        [JsonIgnore]
        public virtual Category Category { get; set; }

        public Advertisement() { }

        public Advertisement(string title, string description, DateTime creationDate, decimal cost, int userId, int categoryId)
        {
            Title = title;
            Description = description;
            CreationDate = creationDate;
            Cost = cost;
            UserId = userId;
            CategoryId = categoryId;
        }
    }
}
